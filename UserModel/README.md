# User Model

### Existing

The project [fanfarehub](https://framagit.org/labrigadedestubes/fanfarehub) is based on the web framework [Django](https://www.djangoproject.com/)

We consider here the default user model of the Django authentication system [Django django.contrib.auth](https://docs.djangoproject.com/en/3.0/topics/auth/default/)
adopted by our project.

![Django User Model](Existing/Output/existing.svg)

It is worth noticing that `username` is required and unique. The Django admin site uses the permissions (`codename`) for any type of object as follows:
- the "view" permission;
- the "change" permission;
- the "add" permission;
- the "delete" permission.

The logical relational data model is as follows :
```
User (_username_, password, email, first_name, last_name, is_staff, is_active, is_superuser, last_login, date_joined)
groups (_#username_, _#name_)
Group (_name_)
user_permissions (_#name_, _#username_)
Permission (_name_, codename, #app_label)
permissions (_#name_, _#name.1_)
ContentType (_app_label_, model, name)
```

### Expected

According to the [draft](http://moonskywalka.free.fr/brigade/site_interne_2020/#id=v4hkmp&p=connexion_inscription&g=1) of the registration/login page and some [discussions](../MeetingMinutes/2020-05-05.md), the entity-relation diagramm (work-in-progress) is as follows:

![FanfareHub User Model](Expected/Output/expected.svg)

In the latter, we observe that:
- the identifying property of a `User` is the `email` and there is no property `username`;
- each `User` plays a single main `Ìnstrument`;
- each `User` may play several other additional  `Ìnstrument`;
- a stand (e.g. trombone, trumpet, percussion, clarinet) is a group ;
- the contributors and the administrators form `Group` with some rights on the resources;
- a commission is a `Group` ;
- a cercle is a set of `Group`s.

There is a bijection between a `User` and a `Profile` which contains usefull contact information and eventually 
a picture. A `Profile` may be registered to the association according to a `Subscription`
which includes a `subscription_date`, i.e when the user has paid for the last 
time in order to be covered by the insurance during the transport.


An instrument `Instrument` is associated to a transposition (e.g. Bb) a key for reading (e.g. G or F). 

The logical relational data model is as follows :
```
plays (_#id_, _#nameInstrument_)
Instrument (_nameInstrument_, transposition, key, #nameGroup)
File (_fileName_, fileDescription, fileField, creationDate, modificationDate, #id)
Profile (_id_, phone, address, postalCode, city, country, job)
User (_id_, email, password, first_name, last_name, is_active, is_superuser, last_login, date_joined, #nameInstrument, #id.1)
groups (_#id_, _#nameGroup_)
Group (_nameGroup_, #nameGroup.1)
subscription (_id_, last_subscription_date, #id.1)
user_permissions (_#name_, _#id_)
Permission (_name_, codename, #app_label)
permissions (_#name_, _#nameGroup_)
ContentType (_app_label_, model, name)
```

### Remarks

It is worth noticing that the model `User` overrides the model
[`AbstractBaseUser`](https://docs.djangoproject.com/fr/3.0/topics/auth/customizing/).
