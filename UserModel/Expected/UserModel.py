from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a user with the given email and password.
        """
        extra_fields.setdefault('is_superuser', False)
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        """
        Creates and saves a superuser with full access to the application.
        """
        extra_fields.setdefault('is_admin', True)
        extra_fields.setdefault('is_superuser', True)
        if extra_fields.get('is_admin') is not True:  # pragma: no cover
            raise ValueError("Superuser must have is_admin=True.")
        if extra_fields.get('is_superuser') is not True:  # pragma: no cover
            raise ValueError("Superuser must have is_superuser=True.")
        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    A user within the application which should correspond to a person and
    optionally a member of the band.

    The email address is used to authenticate the user, thus it must be unique
    and is required with the password. The first and last name are also
    required to identify the person.
    """

    # Identity
    first_name = models.CharField(_("first name"), max_length=150)
    last_name = models.CharField(_("last name"), max_length=150)
    email = models.EmailField(
        _("email address"),
        unique=True,
        error_messages={
            'unique': _("A user with this email address already exists.")
        },
    )

    # Permissions
    is_admin = models.BooleanField(
        _("admin access"),
        default=False,
        help_text=_(
            "Designates whether this user has access to the administration."
        ),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user can log in and should be treated as "
            "active."
        ),
    )

    # Meta
    date_joined = models.DateTimeField(
        _("date joined"), default=timezone.now, editable=False
    )

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    @property
    def is_staff(self):
        # This property is used by the Django admin to designates whether
        # this user has access to the administration, which corresponds
        # to `is_admin` property.
        return self.is_admin

    def __str__(self):
        return self.full_name or self.email

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """Returns the first name plus the last name of this user."""
        return "{} {}".format(self.first_name, self.last_name).strip()

    full_name = property(get_full_name)

    def get_short_name(self):
        """Returns the first name of this user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Sends an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
