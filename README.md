# FanfareHubSpecification

This project collects all the documentations about the [FanfareHub](https://framagit.org/labrigadedestubes/fanfarehub) application:
- [meeting minutes](MeetingMinutes/);
- functional specifications e.g. [usecase](Usecase/);
- technical specifications e.g. [user model](UserModel/);
- deployment guide;
- etc.

## Requirements

[Mocodo](http://mocodo.net) is required for the generation of the entity-relation diagrams.

[PlantUML](https://plantuml.com) is is required for the generation of the usecase diagrams.