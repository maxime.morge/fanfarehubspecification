# Event Model

The application aims at organsing some `Event`s (_prestations_).

An `Event` is described by:
- `nameEvent`, i.e. the name of the event;
- `description`, i.e. the plain text description of the event;
- `duration`, e.g. "1 hour";
- `ready`, i.e. a boolean ;
- `minParticipantNumber`, e.g. 10;
- `maxParticipantNumber`, e.g. the size fo the pullman;


An `Event` belongs to a `Category` (e.g. concert, meeting, workshop, rehearsal). It
is created by an `author` at a `publicationDate`,  i.e. a [`User`](../UserModel/README.md) 
which is a member  of the `commission` called [`prestation`](../UserModel/README.md). 
An `Event` is associated to a set of checkpoints in a roadmap and a set of attachments, i.e.
PDF `File`s (See [Song model](../SongModel/README.md)).  An `Event` has a status (e.g. `option`, `open`, 
`cancel`, `expected`, `confirmed`) and a set of labels (e.g. `amplified`, `inside`, 
`outside`, `march`). An `Event` is organised by at least two contact persons: 
1. a `logisticContact`, i.e. a `User` ; and 
2. an `artisticContact`, i.e. a `User`.

Each `User` replies to an event (e.g go, nogo, may be).

The occurrences and the meeting places of `Event`s are such as  :
- `namePlace`;
- `adress`;
- `postalCode`
- `country`;
- `comment`.

![Event Model](Output/eventModel.svg)


# TODOs

We need to specify the data type of the properties.