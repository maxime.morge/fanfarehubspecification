'java -jar plantuml.jar media.txt
@startuml
left to right direction
skinparam monochrome true
skinparam shadowing false
skinparam packageStyle rectangle
actor user
actor contributor
actor admin
user <|-- contributor
contributor <|-- admin
rectangle fanfareHub {
user -- (Read media resource)
contributor -- (Modify media resource)
admin -- (Add media resource)
admin -- (Remove media resource)
}
@enduml