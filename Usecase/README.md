# Usercase

The aim of this document is to provide an exhaustive list of actors (users) and an exhaustive list of usecases (functionalities) for the [FanfareHub](https://framagit.org/labrigadedestubes/fanfarehub) application.

## Actors

### Django

The project [fanfarehub](https://framagit.org/labrigadedestubes/fanfarehub) is based on the web framework [Django](https://www.djangoproject.com/).

This framework distinguishes two kind of specific users :
1. 'superusers' having all permissions without explicitly assigning them;
2. 'staff' users (also called admin) who can access the admin site.

### Requirements

The users handle resources :
- some usesr can "view" some resources;
- some contributors can "modify" some ressources;
- some 'staff' users can "create" and/or "delete" the ressources.

The users are partitioned in stands, i.e. groups of users based on their instruments as specified in the following table:

| **Stand** | **Instruments**       |
|-----------|-----------------------|
| bass      |  sousaphone, tuba     |
| clarinet  | clarinet              |
| euphonium | euphonium             | 
| drum      | snareDrum, bassDrum   |
| alto      | altoSaxophone         | 
| tenor     | tenorSaxophone       |
| baryton   | barytonSaxophone     | 
| trombone  | trombone              | 
| trumpet   | trumpet, bugle        | 


In order to refine this global organisation, the user can be member of groups (called *commission*) 
which are part of supergroups (called *cercle*) as described in the following table.

| **Cercle**    | **Commissions**                                                          |
|---------------|--------------------------------------------------------------------------|
| diffusion     | prospection, tourneeEte                                                  |
| administratif | conseilAdministration, secrétariat, trésorerie, adhesion, preparationAG  |
| instrument    |  entretien, prêt                                                         |
| logistique    |  partenaire, materiel, prestation                                        |
| artistique    |  direction, partition, repertoire, standBass, standClarinet, ...         |
| communication | interne, externe                                                         |
| événement     |  fete, concertHiver, repetitionPublique                                  |


## Usecases

### Basic usecases

The basic usecases are related to the authentification.

![Authentification](authentification.png)
 
Even if this two ressources are accessible to any user (which are not necessarily connected yet), 
the access to the other ressources required the authentification.

 ### Media resources
 
The application allow sharing some media resources for each song (cf. [Song Model](../SongModel/README.md)).
Moreover, the application allows sharing some media resources for the management fo the band :
- the audio recordings of the gigs (.mp3);
- the video recordings of the gigs (.mp4);
- the pictures of the gigs (.jpg);
- the logos (.png .gif);
- the minutes meetings, the rules, etc. (.odt .pdf)

Remark: No decision have been taken about the usage of  a [nextcloud](https://nextcloud.com/) instance.

![Media ressource](media.png)

 ### Event

The application allow managing [events](../EventModel/README.md).
An event  is created by an `author`, i.e. a [`User`](../UserModel/README.md) 
which is a member  of the `commission` called [`prestation`](../UserModel/README.md).  
The event can be modify by one of the user which is designated as a logistic contact
or as an artitstic contact. All the registered users can read and reply to the event with
an answer (og, nogo, maybe).

![Event usecases](event.png)

## TODOs

We need to specify :
- how to explore the repertoire;
- how to manage an event;
-the loan process.