# Compte-rendu de la réunion fanfarehub du 5 mai 2020 18h-19h

## Participants

Présents : Fred, Jérôme, Maxime, Mokhtar, Romain  

Excusés : Julien 

Invités surprise : Mounir 

## Point sur l'avancement

Fred intègre le projet et propose un coup de main notamment pour les tests. Bienvenue Fred !

Jérôme et Romain ont travaillé sur l'authentification. 

Maxime a fait des retours sur les propositions.

Mokhtar a fait de la veille pour identifier les modules pertinents, notamment pour l'interface.

Mounir informe l'équipe qu'il a trouvé un web designer à intégrer.

## Spécification fonctionnelle

Mounir rappel qu'une [maquette](http://moonskywalka.free.fr/brigade/site_interne_2020) du site a été réalisé.


L'équipe de développement s'interroge sur le modèle de données à implémenter. 
Maxime se charge de faire une première version du [modèle d'utilisateur](../UserModel/README.md)  en fonction :
- de l'existant dans Django ;
- des informations données par Mounir ;
- de la discussion.

Dans cette dernière, il a été mentionné que :
- l'identification se fait par mail ;
- chaque utilisateur a un instrument principal ;
- chaque utilisateur fait partie d'un ou plusieurs pupitres ;
- chaque pupitre de la brigade regroupe un ou plusieurs instruments ;
- les utilisateurs sont (ou pas) à jour de cotisation ;
- le prêt d'instrument pourrait être généré par l'application ;
- l'application est animée par des contributeurs qui peuvent abonder les resources ;
- l'application est structurée par des administrateurs qui peuvent créer et supprimer des resources ;
- les droits des utilisateurs sont définis par l'organisation en commissions et en cercles (i.e. ensemble de commissions) de la brigade.

## Use case

Mokhtar a proposé un premier de [diagramme de cas d'usage](Annexes/useCase2020-05-05.pdf) (en anglais, _use case_) pour énumérer l'ensemble des acteurs et des fonctionnalités.

##  Modules

Jérôme propose d'intégrer dès le début du projet le framework CSS [Bulma](https://bulma.io/) pour concevoir l'interface. Aucune
objection n'a été soulevée. Ce choix pourra être remis en cause par la suite par le web designer.

Jérôme propose d'intégrer le module [django-tapeforms](https://github.com/stephrdev/django-tapeforms) pour le rendu de formulaire. La question de la compatibilité avec 
Bulma ou [Bootstrap](https://getbootstrap.com) est levée.

## Conclusion

Romain accompagne Fred dans sa découverte du projet en lui pointant un [tutorial](https://tutorial.djangogirls.org/fr).

Rendez-vous est pris pour le mardi 12 mai de 18h à 19h.

