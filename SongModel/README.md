# Song Model

The application aims at sharing some media resources for each `Song` (_morceau_) in the music repertoire 
and for each `Part` (_partie_) of these songs such as:  
- the scores (_partitions_) for each stand (_pupitre_) and the lead sheet (_conducteur_) in the pdf format;
- the MuseScore original files in the mscz format;
- the midi files in the midi format;
- the full audio files in the mp3 format;
- the audio file of each part in the mp3 format;
- the audio file of the original song in the mp3 format;
- the audio recording in the mp3 format;
- the video recording in the mp4 format;
- the comments in the odt format;

A `File` is described by:
- `fileName`, i.e. the name of the file;
- `description`, i.e. the plain text description of the file; 
- `fileField`, i.e. the [django.models.FileField](https://docs.djangoproject.com/fr/3.0/topics/files/);
- `creationDate`, i.e. a timestamp.
- `modificationDate`, i.e. a timestamp.

Each `File` has a `Filetype` which could be `mscz`, `mid`, `mp3`, `mp4`, `odt` or `pdf`.

A `Song` is described by:
- `titleSong`, i.e. the title of the song ;
- `composer`, i.e. the composer of the song;
- `arranger`, i.e. the arranger of the song;
- `description`, i.e. the description of the song ;

A `Song` is built upon one part for each `Stand`, i.e. 
the [group of instruments](../UserModel/README.md#expected) which plays it.

A `Song` is described by :
- at most one `originalFile`, i.e. the orignal audio or video file;
- at most one `sourceFile`, i.e. the corresponding MuseScoreFile;
- at most one `scoreFile`, i.e. the pdf file; 
- none or several `audioFile`, i.e. the midi/mp3 audio files;
- at most one `audioRecording`, i.e. the audio recording;
- at most one `videoRecording`, i.e. the video recording;
- at most one `comment`, i.e. the file desribing the choreography.

a `Part` for a `Stand` is described by at most :
- at most one `scoreFile`, the corresponding pdf file;
- at most one `audioFile`, the audio file;

A `SetList` (_liste de morceau_) is an ordered set of songs with : 
- `nameSetList`, i.e. the name of the setlist;
- `creationDate`, i.e. a date;
- `lastModificationDate`, i.e. a date.

A `Song` may belong to several `Setlist`. A `Setlist` can played for an [`Event`](../EventModel/README.md). 
A cluster is a `Setlist` which is not necessairly played for an `Event`.

![Song Model](Output/songModel.svg)

The logical relational data model is as follows :
```
FileType (_fileTypeName_)
File (_fileName_, fileDescription, fileField, creationDate, modificationDate, #fileTypeName)
audioOf (_#title_, _#fileName_)
Song (_title_, description, composer, arranger, #fileName, #fileName.1, #fileName.2, #fileName.3, #fileName.4, #name, #fileName.5, #fileName.6)
Stand (_name_, #title, #fileName)
contains (_#title_, _#nameSetList_, number)
SetList (_nameSetList_, creationDate, lastModificationDate)
Event (_nameEvent_, ..., #nameSetList)
```

# TODOs

We need to specify the data type of the properties.